// SPDX-License-Identifier: GPL-3.0-only
#include <cmath>
#include "pluginterfaces/vst/ivstparameterchanges.h"
#include "myvst3fuid.h"
#include "myvst3define.h"
#include "processor.h"

namespace Steinberg {
	namespace Vst {
		template<class T>
		ParamValue inline static getvalue(IParamValueQueue* queue)
		{
			ParamValue value;
			int32 sampleOffset;
			if (queue->getPoint(queue->getPointCount() - 1, sampleOffset, value) == kResultTrue)
			{
				return (T::Max-T::Min)*value + T::Min;
			}
			return T::Def;
		}

		MyVSTProcessor::MyVSTProcessor() : processor()
		{
			setControllerClass(ControllerUID);
		}

		tresult PLUGIN_API MyVSTProcessor::initialize(FUnknown* context)
		{
			tresult result = AudioEffect::initialize(context);
			if (result != kResultTrue) return result;
			addAudioInput(STR16("AudioInput"), SpeakerArr::kMono);
			addAudioOutput(STR16("AudioOutput"), SpeakerArr::kMono);
			processor.formant(Formant::Def);
			processor.pitch(Pitch::Def);
			processor.threshold(std::powf(10.0f, Threshold::Def));
			processor.low(Low::Def);
			processor.high(High::Def);
			return kResultTrue;
		}

		tresult PLUGIN_API MyVSTProcessor::setBusArrangements(SpeakerArrangement* inputs, int32 numIns, SpeakerArrangement* outputs, int32 numOuts)
		{
			if (numIns == 1 && numOuts == 1 && inputs[0] == SpeakerArr::kMono && outputs[0] == SpeakerArr::kMono)
				return AudioEffect::setBusArrangements(inputs, numIns, outputs, numOuts);
			return kResultFalse;
		}

		void MyVSTProcessor::updateParam(ProcessData& data)
		{
			if (data.inputParameterChanges == NULL) return;

			int32 paramChangeCount = data.inputParameterChanges->getParameterCount();
			for (int32 i = 0; i < paramChangeCount; i++)
			{
				IParamValueQueue* queue = data.inputParameterChanges->getParameterData(i);
				if (queue == NULL) continue;

				switch (queue->getParameterId())
				{
				case TAG::Pitch: processor.pitch(getvalue<Pitch>(queue)); break;
				case TAG::Formant: processor.formant(getvalue<Formant>(queue)); break;
				case TAG::Threshold: processor.threshold(std::pow(10.0, getvalue<Threshold>(queue))); break;
				case TAG::Low: processor.low(getvalue<Low>(queue)); break;
				case TAG::High: processor.high(getvalue<High>(queue)); break;
				}
			}
		}

		tresult PLUGIN_API MyVSTProcessor::process(ProcessData& data)
		{
			updateParam(data);

			Sample32* in = data.inputs[0].channelBuffers32[0];
			Sample32* out = data.outputs[0].channelBuffers32[0];
			processor.Process(in, out, data.numSamples);
			return kResultTrue;
		}

	}
} // end namaespace
