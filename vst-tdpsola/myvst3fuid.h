// SPDX-License-Identifier: GPL-3.0-only
#pragma once
#include "pluginterfaces\base\funknown.h"

namespace Steinberg {
	namespace Vst {
		static const FUID ProcessorUID(0x41FFAB82, 0xBBEB4D4B, 0x90EA1121, 0xB5FA5ED7);
		static const FUID ControllerUID(0x6E780DF7, 0x43CB4792, 0x82E48FBA, 0x0DC40F22);
	}
}
