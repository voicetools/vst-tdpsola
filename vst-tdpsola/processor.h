// SPDX-License-Identifier: GPL-3.0-only
#pragma once
#include "public.sdk/source/vst/vstaudioeffect.h"
#include "Algorithm.h"

namespace Steinberg {
	namespace Vst {

		class MyVSTProcessor : public AudioEffect
		{
		private:
			AudioProcessor::Processor processor;

			MyVSTProcessor();
			void updateParam(ProcessData&);
		public:

			tresult PLUGIN_API initialize(FUnknown* context);
			tresult PLUGIN_API setBusArrangements(SpeakerArrangement* inputs, int32 numIns, SpeakerArrangement* outputs, int32 numOuts);
			tresult PLUGIN_API process(ProcessData& data);
			static FUnknown* createInstance(void*) { return (IAudioProcessor*)new MyVSTProcessor(); }
		};
	}
}
