// SPDX-License-Identifier: GPL-3.0-only
#pragma once
#include "public.sdk/source/vst/vsteditcontroller.h"
#include "myvst3define.h"

namespace Steinberg {
	namespace Vst {
		class MyVSTController : public EditController
		{
		public:
			tresult PLUGIN_API initialize(FUnknown* context) {
				tresult result = EditController::initialize(context);
				if (result != kResultTrue) return result;
				auto pitch = new RangeParameter(STR16("pitch"), TAG::Pitch, nullptr, Pitch::Min, Pitch::Max, Pitch::Def);
				parameters.addParameter(pitch);
				auto formant = new RangeParameter(STR16("formant"), TAG::Formant, nullptr, Formant::Min, Formant::Max, Formant::Def);
				parameters.addParameter(formant);
				auto threshold = new RangeParameter(STR16("threshold"), TAG::Threshold, nullptr, Threshold::Min, Threshold::Max, Threshold::Def);
				threshold->setPrecision(2);
				parameters.addParameter(threshold);
				auto low = new RangeParameter(STR16("low"), TAG::Low, STR16("Hz"), Low::Min, Low::Max, Low::Def);
				low->setPrecision(1);
				parameters.addParameter(low);
				auto high = new RangeParameter(STR16("high"), TAG::High, STR16("Hz"), High::Min, High::Max, High::Def);
				high->setPrecision(1);
				parameters.addParameter(high);

				return kResultTrue;
			}
			static FUnknown* createInstance(void*) { return (IEditController*)new MyVSTController(); }
		};
	}
}
