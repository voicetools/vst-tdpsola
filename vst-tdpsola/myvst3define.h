// SPDX-License-Identifier: GPL-3.0-only
#pragma once
#include "pluginterfaces\vst\vsttypes.h"

struct TAG {
	enum : Steinberg::Vst::ParamID {
		Pitch,
		Formant,
		Threshold,
		Low,
		High,
	};
};

struct Pitch {
	static constexpr Steinberg::Vst::ParamValue Min = 0.3, Max = 2.0, Def = 1;
};

struct Formant {
	static constexpr Steinberg::Vst::ParamValue Min = 0.3, Max = 2.0, Def = 1;
};

struct Threshold {
	static constexpr Steinberg::Vst::ParamValue Min = -6.0, Max = +1.0, Def = -5.0;
};

struct Low {
	static constexpr Steinberg::Vst::ParamValue Min = 50, Max = 250, Def = 80;
};

struct High {
	static constexpr Steinberg::Vst::ParamValue Min = 250, Max = 800, Def = 500;
};