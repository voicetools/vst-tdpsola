# vst-tdpsola

vst-tdpsolaはリアルタイム音声信号処理としてTD-PSOLAを実装したVSTプラグインです。

## License

このソフトウェアはVST3 SDKのLicenseに基づき[GPLv3](http://www.gnu.org/licenses/gpl-3.0.html)により配布しています。

## 使い方

releaseページからVSTプラグイン本体をインストール


## build 方法

http://vstcpp.wpblog.jp/ を参考にVSTプラグインを作成しております。

VST SDK自体のビルドについてはこちらをご参照ください。
